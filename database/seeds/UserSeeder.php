<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('users')->get()->count() == 0) {
            DB::table('users')->insert([
                'name'         => 'admin',
                'phone_number' => '8989898989',
                'address'      => 'admin street',
                'email'        => 'admin@admin.com',
                'password'     => bcrypt('12345678'),
                'created_at'   => now()
            ]);
        } else {
            echo "Table is not empty";
        }
    }
}
