<?php

namespace App\Http\Controllers\API\Bannerimage;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/* Banner */
use App\Services\Banner\BannerService;
use Illuminate\Support\Facades\Session;


class BannerApiController extends Controller
{

    /**
     * @var BannerService
     */
    private $BannerService;

    public function __construct()
    {
        $this->BannerService            = new BannerService();
    }

    /**
     * @OA\Get(
     * path="/banner",
     * @OA\SecurityScheme(
     *      securityScheme="bearerAuth",
     *      in="header",
     *      name="bearerAuth",
     *      type="http",
     *      scheme="bearer",
     *      bearerFormat="JWT",
     * ),

     * summary="Get Banner image Details",
     * description="Get Banner Image Details",
     * operationId="Banner",
     * tags={"BannerImage"},
     * @OA\Response(
     *    response=422,description="Wrong credentials response",
     *     )
     * )
     */

    /**
     * BANNER INDEX SCREEN.
     *
     */
    public function getBanner()
    {
        $details['banner'] = $this->BannerService->getBannerList();
        $result = Helper::customResponse(200, $details, null, config('constants.ServiceName.Dashboard'), 1600);
        return response()->json($result, 200);
    }


}
