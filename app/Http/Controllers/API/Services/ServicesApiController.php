<?php

namespace App\Http\Controllers\API\Services;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/* Services */
use App\Services\Services\ServicesService;
use Illuminate\Support\Facades\Session;


class ServicesApiController extends Controller
{

    /**
     * @var ServicesService
     */
    private $ServicesService;

    public function __construct()
    {
        $this->ServicesService            = new ServicesService();
    }

    /**
     * @OA\Get(
     * path="/services",
     * @OA\SecurityScheme(
     *      securityScheme="bearerAuth",
     *      in="header",
     *      name="bearerAuth",
     *      type="http",
     *      scheme="bearer",
     *      bearerFormat="JWT",
     * ),

     * summary="Get Services Details",
     * description="Get Services Details",
     * operationId="Services",
     * tags={"Services"},
     * @OA\Response(
     *    response=422,description="Wrong credentials response",
     *     )
     * )
     */

    /**
     * SERVICES INDEX SCREEN.
     *
     */
    public function getServices()
    {
        $details['services'] = $this->ServicesService->getServicesList();
        $result = Helper::customResponse(200, $details, null, config('constants.ServiceName.Dashboard'), 1600);
        return response()->json($result, 200);
    }


}
