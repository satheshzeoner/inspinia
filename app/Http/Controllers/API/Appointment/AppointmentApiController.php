<?php

namespace App\Http\Controllers\API\Appointment;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

/* Services */

use App\Services\Appointment\AppointmentService;

class AppointmentApiController extends Controller
{

    /**
     * @var AppointmentService
     */
    private $AppointmentService;

    public function __construct()
    {
        $this->AppointmentService = new AppointmentService();
    }

    /**
     * @OA\Get(
     * path="/appointment",
     * @OA\SecurityScheme(
     *      securityScheme="bearerAuth",
     *      in="header",
     *      name="bearerAuth",
     *      type="http",
     *      scheme="bearer",
     *      bearerFormat="JWT",
     * ),

     * summary="Get Appointment Details",
     * description="Get Appointment Details",
     * operationId="appointment",
     * tags={"Appointment"},
     * @OA\Response(
     *    response=422,description="Wrong credentials response",
     *     )
     * )
     */

    /**
     * APPOINTMENT GET DETAILS
     *
     */
    public function getAppointment()
    {
        $details['appointment'] = $this->AppointmentService->getAppointmentList();
        $result = Helper::customResponse(200, $details, null, config('constants.ServiceName.Dashboard'), 1600);
        return response()->json($result, 200);
    }


}
