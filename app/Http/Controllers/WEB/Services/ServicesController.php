<?php

namespace App\Http\Controllers\WEB\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/* Services */
use App\Services\Services\ServicesService;
use Illuminate\Support\Facades\Session;


class ServicesController extends Controller
{

    /**
     * @var ServicesService
     */
    private $ServicesService;

    public function __construct()
    {
        $this->ServicesService            = new ServicesService();
    }

    /**
     * SERVICES INDEX SCREEN.
     *
     */
    public function index()
    {
        $details['services'] = $this->ServicesService->getServicesList();
        $details['no']    = 0;
        return view('web.Services.servicesDisplay')->with($details);
    }

    /**
     * SERVICES ADD SCREEN
     *
     */
    public function servicesAdd()
    {
        return view('web.Services.servicesStore');
    }

    /**
     * SERVICES ADD STORE
     *
     */
    public function servicesStore(request $request)
    {
        $this->ServicesService->storeServices($request);
        Session::flash('store', 'Services Added Successfully');
        return redirect('services/services');
    }

    /**
     * SERVICES EDIT SCREEN
     *
     */
    public function servicesEdit($masterId)
    {
        $services['services']       = $this->ServicesService->getServicesEditDetails($masterId);
        return view('web.Services.servicesEdit')->with($services);
    }

    /**
     * USER ROLE UPDATE PROCESS
     *
     */
    public function servicesUpdate(request $request)
    {
        $services['services'] = $this->ServicesService->UpdateServicesDetails($request);
        Session::flash('update', 'Services Update Successfully');
        return redirect('services/services')->with(['success' => 'Registered Succesfully']);
    }

    /**
     * SERVICES DELETE PROCESS
     *
     */
    public function servicesDestroy(request $request)
    {
        $details['services'] = $this->ServicesService->DestroyServicesDetails($request);
        Session::flash('destroy', 'Services deleted Successfully');
        return redirect('services/services')->with(['success' => 'Deleted Succesfully']);
    }
}
