<?php

namespace App\Http\Controllers\WEB\Appointment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

/* Services */
use App\Services\Appointment\AppointmentService;

class AppointmentController extends Controller
{

    /**
     * @var AppointmentService
     */
    private $AppointmentService;

    public function __construct()
    {
        $this->AppointmentService            = new AppointmentService();
    }

    /**
     * APPOINTMENT INDEX SCREEN.
     *
     */
    public function index()
    {
        $details['appointment'] = $this->AppointmentService->getAppointmentList();
        $details['no']    = 0;
        return view('web.Appointment.appointmentDisplay')->with($details);
    }

    /**
     * APPOINTMENT ADD SCREEN
     *
     */
    public function appointmentAdd()
    {
        return view('web.Appointment.appointmentStore');
    }

    /**
     * APPOINTMENT ADD STORE
     *
     */
    public function appointmentStore(request $request)
    {
        $this->AppointmentService->storeAppointment($request);
        Session::flash('store', 'Appointment Added Successfully');
        return redirect('appointment/appointment');
    }

    /**
     * APPOINTMENT EDIT SCREEN
     *
     */
    public function appointmentEdit($masterId)
    {
        $appointment['appointment']       = $this->AppointmentService->getAppointmentEditDetails($masterId);
        return view('web.Appointment.appointmentEdit')->with($appointment);
    }

    /**
     * USER ROLE UPDATE PROCESS
     *
     */
    public function appointmentUpdate(request $request)
    {
        $appointment['appointment'] = $this->AppointmentService->UpdateAppointmentDetails($request);
        Session::flash('update', 'Appointment Update Successfully');
        return redirect('appointment/appointment')->with(['success' => 'Registered Succesfully']);
    }

    /**
     * APPOINTMENT DELETE PROCESS
     *
     */
    public function appointmentDestroy(request $request)
    {
        $details['appointment'] = $this->AppointmentService->DestroyAppointmentDetails($request);
        Session::flash('destroy', 'Appointment deleted Successfully');
        return redirect('appointment/appointment')->with(['success' => 'Deleted Succesfully']);
    }

}
