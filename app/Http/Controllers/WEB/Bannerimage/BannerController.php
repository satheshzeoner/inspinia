<?php

namespace App\Http\Controllers\WEB\Bannerimage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/* Banner */
use App\Services\Banner\BannerService;
use Illuminate\Support\Facades\Session;


class BannerController extends Controller
{

    /**
     * @var BannerService
     */
    private $BannerService;

    public function __construct()
    {
        $this->BannerService            = new BannerService();
    }

    /**
     * BANNER INDEX SCREEN.
     *
     */
    public function index()
    {
        $details['banner'] = $this->BannerService->getBannerList();
        $details['no']    = 0;
        return view('web.Bannerimage.bannerDisplay')->with($details);
    }

    /**
     * BANNER ADD SCREEN
     *
     */
    public function bannerAdd()
    {
        return view('web.Bannerimage.bannerStore');
    }

    /**
     * BANNER ADD STORE
     *
     */
    public function bannerStore(request $request)
    {
        $this->BannerService->storeBanner($request);
        Session::flash('store', 'Banner Added Successfully');
        return redirect('bannerimage/bannerimage');
    }

    /**
     * BANNER EDIT SCREEN
     *
     */
    public function bannerEdit($masterId)
    {
        $banner['banner']       = $this->BannerService->getBannerEditDetails($masterId);
        return view('web.Bannerimage.bannerEdit')->with($banner);
    }

    /**
     * BANNER UPDATE PROCESS
     *
     */
    public function bannerUpdate(request $request)
    {
        $banner['banner'] = $this->BannerService->UpdateBannerDetails($request);
        Session::flash('update', 'Banner Update Successfully');
        return redirect('bannerimage/bannerimage')->with(['success' => 'Registered Succesfully']);
    }

    /**
     * BANNER DELETE PROCESS
     *
     */
    public function bannerDestroy(request $request)
    {
        $details['banner'] = $this->BannerService->DestroyBannerDetails($request);
        Session::flash('destroy', 'Banner deleted Successfully');
        return redirect('bannerimage/bannerimage')->with(['success' => 'Deleted Succesfully']);
    }
}
