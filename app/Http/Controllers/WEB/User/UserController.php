<?php

namespace App\Http\Controllers\WEB\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/* Services */
use App\Services\User\UserService;
use Illuminate\Support\Facades\Session;


class UserController extends Controller
{

    /**
     * @var UserService
     */
    private $UserService;

    public function __construct()
    {
        $this->UserService            = new UserService();
    }

    /**
     * USERS INDEX SCREEN.
     *
     */
    public function index()
    {
        $details['listDetails'] = $this->UserService->getUsersList();
        $details['no']    = 0;
        return view('web.User.userDisplay')->with($details);
    }

    /**
     * USERS ADD SCREEN
     *
     */
    public function usersAdd()
    {
        return view('web.User.userStore');
    }

    /**
     * USERS ADD STORE
     *
     */
    public function usersStore(request $request)
    {
        $this->UserService->storeUser($request);
        Session::flash('store', 'Users Added Successfully');
        return redirect('user/user');
    }

    /**
     * USERS DELETE PROCESS
     *
     */
    public function usersDestroy(request $request)
    {
        $details['services'] = $this->UserService->DestroyUserDetails($request);
        Session::flash('destroy', 'User deleted Successfully');
        return redirect('user/user')->with(['success' => 'Deleted Succesfully']);
    }


}
