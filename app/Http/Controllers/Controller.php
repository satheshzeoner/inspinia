<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use OpenApi\Annotations\OpenApi;


/**
 *
 * @OA\SecurityScheme(
 *      securityScheme="Bearer",
 *      in="header",
 *      name="Authorization",
 *      type="http",
 *      scheme="Bearer",
 * ),
 *
 * @OA\OpenApi(
 *     @OA\Server(
 *         url=L5_SWAGGER_CONST_HOST,
 *         description="API server"
 *     ),

 *     @OA\Info(
 *         version="1.0.0",
 *         title="APi",
 *         description="A sample API that uses a petstore as an example to demonstrate features in the swagger-2.0 specification",
 *         termsOfService="http://swagger.io/terms/",
 *         @OA\Contact(name="Swagger API Team"),
 *         @OA\License(name="MIT")
 *     ),
 * )
 */

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
