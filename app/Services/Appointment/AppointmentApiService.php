<?php

namespace App\Services\Appointment;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Exception;

/* Repository */
use App\Repositories\Appointment\AppointmentRepositories;

class AppointmentApiService
{

    public function __construct()
    {
        $this->AppointmentRepositories = new AppointmentRepositories();
    }

    # =============================================
    # =       ALL DETAILS                         =
    # =============================================

    public function getAppointmentList()
    {
        try {
            return collect($this->AppointmentRepositories->getAppointmentList())->toArray();
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =                 Add Details               =
    # =============================================

    public function storeAppointment($request)
    {
        try {
            $details = $request->all();
            $details['appointment']['user_id'] = Auth::id();
//            dd($details);
            return $this->AppointmentRepositories->storeAppointmentDetails($details['appointment']);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =           Edit Details                    =
    # =============================================

    public function getAppointmentEditDetails($masterId)
    {
        try {
            return $this->AppointmentRepositories->getSpecificAppointmentDetails($masterId);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =                  Update                  =
    # =============================================

    public function UpdateAppointmentDetails(request $request)
    {
        try {
            $updateDetails = $request->all();
            $masterId      = $updateDetails['master_id'];
            return $this->AppointmentRepositories->updateAppointment($masterId, $updateDetails['appointment']);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =             Destroy                       =
    # =============================================

    public function DestroyAppointmentDetails(request $request)
    {
        $postDetails = $request->all();
        try {
            return $this->AppointmentRepositories->destroyAppointment($postDetails['masterid']);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    public function doErrorFormat($responseResult)
    {
        Log::error($responseResult->getMessage());
        $result['status']  = false;
        $result['message'] = $responseResult->getMessage();
        return $result;
    }
}
