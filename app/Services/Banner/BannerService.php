<?php

namespace App\Services\Banner;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Exception;


/* Repository */

use App\Repositories\Banner\BannerRepositories;

class BannerService
{

    public function __construct()
    {
        $this->BannerRepositories = new BannerRepositories();
    }

    # =============================================
    # =       ALL DETAILS                         =
    # =============================================

    public function getBannerList()
    {
        try {
            return collect($this->BannerRepositories->getBannerList())->toArray();
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =                 Add Details               =
    # =============================================

    public function storeBanner($request)
    {
        try {
            $details = $request->all();

            $fileFormatName = $this->uploadFileName($request);

            $details['banner']['image'] = $fileFormatName;

            return $this->BannerRepositories->storeBannerDetails($details['banner']);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =           Edit Details                    =
    # =============================================

    public function getBannerEditDetails($masterId)
    {
        try {
            return $this->BannerRepositories->getSpecificBannerDetails($masterId);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =                  Update                  =
    # =============================================

    public function UpdateBannerDetails(request $request)
    {
        try {
            $updateDetails                     = $request->all();
            $fileFormatName                    = $this->uploadFileName($request);
            $updateDetails['banner']['image'] = $fileFormatName;
            $masterId                          = $updateDetails['master_id'];
            return $this->BannerRepositories->updateBanner($masterId, $updateDetails['banner']);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =             Destroy                       =
    # =============================================

    public function DestroyBannerDetails(request $request)
    {
        $postDetails = $request->all();
        try {
            return $this->BannerRepositories->destroyBanner($postDetails['masterid']);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }


    # =============================================
    # =             FILE UPLOAD                   =
    # =============================================
    public function uploadFileName($request)
    {
        $fileFullName = $request->file('image')->getClientOriginalName();

        $filename  = pathinfo($fileFullName, PATHINFO_FILENAME);
        $extension = pathinfo($fileFullName, PATHINFO_EXTENSION);

        $fileFormatName = $filename . "_" . now()->format('YmdHis') . "." . $extension;
        $path           = $request->file('image')->move(public_path("BannerImage/"), $fileFormatName);

        return $fileFormatName;
    }


    public function doErrorFormat($responseResult)
    {
        Log::error($responseResult->getMessage());
        $result['status']  = false;
        $result['message'] = $responseResult->getMessage();
        return $result;
    }
}
