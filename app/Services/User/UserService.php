<?php

namespace App\Services\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Exception;


/* Repository */

use App\Repositories\User\UserRepositories;

class UserService
{

    public function __construct()
    {
        $this->UserRepositories = new UserRepositories();
    }

    # =============================================
    # =       ALL DETAILS                         =
    # =============================================

    public function getUsersList()
    {
        try {
            return collect($this->UserRepositories->getUserList())->toArray();
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =                 Add Details               =
    # =============================================

    public function storeUser($request)
    {
        try {
            $user                     = $request->all();
            $user['user']['password'] = Hash::make($user['user']['password']);
            return $this->UserRepositories->storeUserDetails($user['user']);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =             Destroy                       =
    # =============================================

    public function DestroyUserDetails(request $request)
    {
        $postDetails = $request->all();
        try {
            return $this->UserRepositories->destroyUser($postDetails['masterid']);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }


    public function doErrorFormat($responseResult)
    {
        Log::error($responseResult->getMessage());
        $result['status']  = false;
        $result['message'] = $responseResult->getMessage();
        return $result;
    }
}
