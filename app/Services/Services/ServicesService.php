<?php

namespace App\Services\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Exception;


/* Repository */

use App\Repositories\Services\ServicesRepositories;

class ServicesService
{

    public function __construct()
    {
        $this->ServicesRepositories = new ServicesRepositories();
    }

    # =============================================
    # =       ALL DETAILS                         =
    # =============================================

    public function getServicesList()
    {
        try {
            return collect($this->ServicesRepositories->getServicesList())->toArray();
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =                 Add Details               =
    # =============================================

    public function storeServices($request)
    {
        try {
            $details = $request->all();

            $fileFormatName = $this->uploadFileName($request);

            $details['service']['image'] = $fileFormatName;

            return $this->ServicesRepositories->storeServicesDetails($details['service']);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =           Edit Details                    =
    # =============================================

    public function getServicesEditDetails($masterId)
    {
        try {
            return $this->ServicesRepositories->getSpecificServicesDetails($masterId);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =                  Update                  =
    # =============================================

    public function UpdateServicesDetails(request $request)
    {
        try {
            $updateDetails                     = $request->all();
            $fileFormatName                    = $this->uploadFileName($request);
            $updateDetails['service']['image'] = $fileFormatName;
            $masterId                          = $updateDetails['master_id'];
            return $this->ServicesRepositories->updateServices($masterId, $updateDetails['service']);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }

    # =============================================
    # =             Destroy                       =
    # =============================================

    public function DestroyServicesDetails(request $request)
    {
        $postDetails = $request->all();
        try {
            return $this->ServicesRepositories->destroyServices($postDetails['masterid']);
        } catch (Exception $e) {
            return $this->doErrorFormat($e);
        }
    }


    # =============================================
    # =             FILE UPLOAD                   =
    # =============================================
    public function uploadFileName($request)
    {
        $fileFullName = $request->file('image')->getClientOriginalName();

        $filename  = pathinfo($fileFullName, PATHINFO_FILENAME);
        $extension = pathinfo($fileFullName, PATHINFO_EXTENSION);

        $fileFormatName = $filename . "_" . now()->format('YmdHis') . "." . $extension;
        $path           = $request->file('image')->move(public_path("ServiceImage/"), $fileFormatName);

        return $fileFormatName;
    }


    public function doErrorFormat($responseResult)
    {
        Log::error($responseResult->getMessage());
        $result['status']  = false;
        $result['message'] = $responseResult->getMessage();
        return $result;
    }
}
