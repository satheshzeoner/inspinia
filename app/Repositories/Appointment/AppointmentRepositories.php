<?php

namespace App\Repositories\Appointment;

/* Models */

use App\Models\Appointment\Appointment;
use Illuminate\Support\Facades\Log;

class AppointmentRepositories
{
    /* ===============================CREATE=============================== */

    /* Get Create Details */
    public function storeAppointmentDetails($Details)
    {
        $detailsResult = Appointment::create($Details);
        Log::info('Appointments Details has Inserted :' . $detailsResult->id);
        return $detailsResult->id;
    }

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* Get Appointment Details */
    public function getAppointmentList()
    {
        return Appointment::orderBy('id', 'DESC')->get();
    }

    /* Get Specific Appointment Details */
    public function getSpecificAppointmentDetails($masterId)
    {
        return Appointment::whereId($masterId)->first();
    }

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* Get Specific Appointment Details */
    public function updateAppointment($masterId, $details)
    {
        return Appointment::whereId($masterId)->update($details);
    }

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* Get Specific Appointment Details */
    public function destroyAppointment($masterId)
    {
        return Appointment::whereId($masterId)->delete();
    }

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
