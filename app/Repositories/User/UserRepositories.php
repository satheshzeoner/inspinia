<?php

namespace App\Repositories\User;

/* Models */

use App\Models\User;
use Illuminate\Support\Facades\Log;

class UserRepositories
{
    /* ===============================CREATE=============================== */

    /* Get Create Details */
    public function storeUserDetails($Details)
    {
        $detailsResult = User::create($Details);
        Log::info('User has Inserted :' . $detailsResult->id);
        return $detailsResult->id;
    }

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* Get Users Details */
    public function getUserList()
    {
        return User::get();
    }

    /* Get Specific Enquiry Details */
    public function getSpecificEnquiryDetails($masterId)
    {
        return Enquiry::whereId($masterId)->first();
    }

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* Get Specific Enquiry Details */
    public function updateEnquiry($masterId, $details)
    {
        return Enquiry::whereId($masterId)->update($details);
    }

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* Get Specific Enquiry Details */
    public function destroyUser($masterId)
    {
        return User::whereId($masterId)->delete();
    }

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
