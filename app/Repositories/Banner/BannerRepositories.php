<?php

namespace App\Repositories\Banner;

/* Models */

use App\Models\Banner\Banner;
use Illuminate\Support\Facades\Log;

class BannerRepositories
{
    /* ===============================CREATE=============================== */

    /* Get Create Details */
    public function storeBannerDetails($Details)
    {
        $detailsResult = Banner::create($Details);
        Log::info('Banner Details has Inserted :' . $detailsResult->id);
        return $detailsResult->id;
    }

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* Get Banner Details */
    public function getBannerList()
    {
        return Banner::orderBy('id', 'DESC')->get();
    }

    /* Get Specific Banner Details */
    public function getSpecificBannerDetails($masterId)
    {
        return Banner::whereId($masterId)->first();
    }

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* Get Specific Banner Details */
    public function updateBanner($masterId, $details)
    {
        return Banner::whereId($masterId)->update($details);
    }

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* Get Specific Banner Details */
    public function destroyBanner($masterId)
    {
        return Banner::whereId($masterId)->delete();
    }

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
