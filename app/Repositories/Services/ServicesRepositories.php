<?php

namespace App\Repositories\Services;

/* Models */

use App\Models\Services\Services;
use Illuminate\Support\Facades\Log;

class ServicesRepositories
{
    /* ===============================CREATE=============================== */

    /* Get Create Details */
    public function storeServicesDetails($Details)
    {
        $detailsResult = Services::create($Details);
        Log::info('Servicess Details has Inserted :' . $detailsResult->id);
        return $detailsResult->id;
    }

    /* ===============================CREATE-END=========================== */

    /* ===============================READ================================= */

    /* Get Services Details */
    public function getServicesList()
    {
        return Services::orderBy('id', 'DESC')->get();
    }

    /* Get Specific Services Details */
    public function getSpecificServicesDetails($masterId)
    {
        return Services::whereId($masterId)->first();
    }

    /* ===============================READ-END============================ */

    /* ===============================UPDATE=============================== */

    /* Get Specific Services Details */
    public function updateServices($masterId, $details)
    {
        return Services::whereId($masterId)->update($details);
    }

    /* ===============================UPDATE-END========================== */

    /* ===============================DELETE=============================== */

    /* Get Specific Services Details */
    public function destroyServices($masterId)
    {
        return Services::whereId($masterId)->delete();
    }

    /* ===============================DELETE-END========================== */


    /* ===============================FORMATTING========================== */

    /* ===============================FORMATTING-END====================== */
}
