<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    Route::group(['prefix' => 'appointment'], function () {

        /* APPOINTMENT VIEW */
        Route::get('/appointment', 'WEB\Appointment\AppointmentController@index')->name('appointment');

        /* APPOINTMENT ADD */
        Route::get('/appointment-add', 'WEB\Appointment\AppointmentController@appointmentAdd')->name('appointment-add');
        Route::post('/appointment-store', 'WEB\Appointment\AppointmentController@appointmentStore')->name('appointment-store');

        /* APPOINTMENT EDIT */
        Route::get('/appointment-edit/{id}', 'WEB\Appointment\AppointmentController@appointmentEdit')->name('appointment-edit');
        Route::post('/appointment-update', 'WEB\Appointment\AppointmentController@appointmentUpdate')->name('appointment-update');

        /* SERVICES DELETE*/
        Route::post('/appointment-destroy', 'WEB\Appointment\AppointmentController@appointmentDestroy')->name('appointment-destroy');

    });

    Route::group(['prefix' => 'services'], function () {

        /* SERVICES INDEX*/
        Route::get('/services', 'WEB\Services\ServicesController@index')->name('services');

        /* SERVICES ADD */
        Route::get('/services-add', 'WEB\Services\ServicesController@servicesAdd')->name('services-add');
        Route::post('/services-store', 'WEB\Services\ServicesController@servicesStore')->name('services-store');

        /* SERVICES EDIT */
        Route::get('/services-edit/{id}', 'WEB\Services\ServicesController@servicesEdit')->name('services-edit');
        Route::post('/services-update', 'WEB\Services\ServicesController@servicesUpdate')->name('services-update');

        /* SERVICES DELETE*/
        Route::post('/services-destroy', 'WEB\Services\ServicesController@servicesDestroy')->name('services-destroy');


    });

    Route::group(['prefix' => 'bannerimage'], function () {

        /* BANNER INDEX */
        Route::get('/bannerimage', 'WEB\Bannerimage\BannerController@index')->name('bannerimage');

        /* BANNER ADD */
        Route::get('/banner-add', 'WEB\Bannerimage\BannerController@bannerAdd')->name('banner-add');
        Route::post('/banner-store', 'WEB\Bannerimage\BannerController@bannerStore')->name('banner-store');

        /* BANNER EDIT */
        Route::get('/banner-edit/{id}', 'WEB\Bannerimage\BannerController@bannerEdit')->name('banner-edit');
        Route::post('/banner-update', 'WEB\Bannerimage\BannerController@bannerUpdate')->name('banner-update');

        /* BANNER DELETE*/
        Route::post('/banner-destroy', 'WEB\Bannerimage\BannerController@bannerDestroy')->name('banner-destroy');


    });

    Route::group(['prefix' => 'user'], function () {

        /* USER INDEX */
        Route::get('/user', 'WEB\User\UserController@index')->name('user');

        /* USER ADD */
        Route::get('/user-add', 'WEB\User\UserController@usersAdd')->name('user-add');
        Route::post('/user-store', 'WEB\User\UserController@usersStore')->name('user-store');

        /* USER DELETE*/
        Route::post('/user-destroy', 'WEB\User\UserController@usersDestroy')->name('users-destroy');


    });



});
