@extends('layouts.app')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Banner Image</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Banner Image</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Banner Image</h5>
                    </div>
                    <div class="ibox-content">
                        <form role="form" id="form1" method="POST" action="{{ route('banner-store') }}" class="validate" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row"><label class="col-lg-2 col-form-label control-label">Banner Description</label>

                                <div class="col-lg-10"><input type="text" placeholder="Banner Description" name="banner[banner_description]" class="form-control" value="" required>
                                </div>
                            </div>

                            <div class="form-group row"><label class="col-lg-2 col-form-label control-label">Banner Position</label>

                                <div class="col-lg-10"><input type="text" placeholder="Banner Position" name="banner[position]" class="form-control" value="" required>
                                </div>
                            </div>

                            <div class="form-group row"><label class="col-lg-2 col-form-label">Image</label>

                                <div class="col-lg-10">
                                    <div class="custom-file">
                                        <input id="logo" type="file" class="custom-file-input" name="image">
                                        <label for="logo" class="custom-file-label">Choose file...</label>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-sm btn-success" type="submit">Submit</button>
                                    <a class="btn btn-sm btn-info" type="button" href="{{ route('bannerimage') }}">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('pagescript')
    <script>
    $().ready(function() {
        // validate the comment form when it is submitted
        $(".validate").validate();
    });

    $('.custom-file-input').on('change', function() {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
    });
    </script>
@endsection
