@extends('layouts.app')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Services</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Services</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Services List</h5>
                        <div class="ibox-tools">
                            <a class="btn btn-sm btn-primary" style="float: right"
                               href="{{ route('services-add') }}">Add Services</a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Services Name</th>
                                    <th class="text-center">Image</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($services as $row)
                                    <tr>
                                        <td>{{ ++$no }}</td>
                                        <td>{{ $row['service_name'] }}</td>
                                        <td class="text-center">
                                            <img src="{{url('ServiceImage/'.$row['image'])}}" alt="" width="25%">

                                        </td>
                                        <td class="text-center">
                                            <a href="{{ url('services/services-edit/'.$row['id']) }}"
                                               class="btn btn-primary">
                                                <i class="fa fa-pencil"></i>
                                            </a>

                                            <button type="button" data-toggle="modal" class="btn btn-danger"
                                                    href="#modal-form"
                                                    onclick="deleteAction({{ $row['id'] }})">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>S.No</th>
                                    <th>Services Name</th>
                                    <th>Image</th>
                                    <th class="text-center">Action</th>

                                </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>




    <div id="modal-form" class="modal fade" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('services-destroy') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            Are You Sure You Want to Delete ?
                            <input type="hidden" class="masterid" name="masterid" value="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Yes</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection


@section('pagescript')

    <script>
        $(document).ready(function () {
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
        });


        function deleteAction(id) {
            $('.masterid').val(id);
        }

    </script>

@endsection
