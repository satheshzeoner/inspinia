@extends('layouts.app')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>User</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>User</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>User</h5>
                    </div>
                    <div class="ibox-content">
                        <form role="form" id="form1" method="POST" action="{{ route('user-store') }}" class="validate" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row"><label class="col-lg-2 col-form-label control-label">{{ __('Name') }}</label>
                                <div class="col-lg-10"><input type="text" placeholder="Name" name="user[name]" class="form-control" value="" required>                                </div>
                            </div>

                            <div class="form-group row"><label class="col-lg-2 col-form-label control-label">{{ __('Phone Number') }}</label>
                                <div class="col-lg-10"><input type="text" placeholder="Phone Number" name="user[phone_number]" class="form-control" value="" required>                                </div>
                            </div>

                            <div class="form-group row"><label class="col-lg-2 col-form-label control-label">{{ __('Address') }}</label>
                                <div class="col-lg-10"><input type="text" placeholder="Address" name="user[address]" class="form-control" value="" required>                                </div>
                            </div>

                            <div class="form-group row"><label class="col-lg-2 col-form-label control-label">{{ __('E-Mail Address') }}</label>
                                <div class="col-lg-10"><input type="text" placeholder="E-Mail Address" name="user[email]" class="form-control" value="" required>
                                </div>
                            </div>

                            <div class="form-group row"><label class="col-lg-2 col-form-label control-label">{{ __('Password') }}</label>
                                <div class="col-lg-10"><input type="text" placeholder="Password" name="user[password]" class="form-control" value="" required>
                                </div>
                            </div>

                            <div class="form-group row"><label class="col-lg-2 col-form-label control-label">{{ __('Repeat Password') }}</label>
                                <div class="col-lg-10"><input type="text" placeholder="Re-type password"  class="form-control" value="" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-sm btn-success" type="submit">Submit</button>
                                    <a class="btn btn-sm btn-info" type="button" href="{{ route('user') }}">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('pagescript')
    <script>
    $().ready(function() {
        // validate the comment form when it is submitted
        $(".validate").validate();
    });

    $('.custom-file-input').on('change', function() {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
    });
    </script>
@endsection
