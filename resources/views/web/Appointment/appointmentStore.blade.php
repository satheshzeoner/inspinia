@extends('layouts.app')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Appointment</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('') }}">Home</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Appointment</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Appointment</h5>
                    </div>
                    <div class="ibox-content">
                        <form role="form" id="form1" method="POST" action="{{ route('appointment-store') }}" class="validate">
                            @csrf

                            <div class="form-group row"><label class="col-lg-2 col-form-label control-label">Appointment Name</label>

                                <div class="col-lg-10"><input type="text" placeholder="Appointment Name" name="appointment[appointment_name]" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row"><label class="col-lg-2 col-form-label">Address</label>

                                <div class="col-lg-10"><input type="text" placeholder="Address" name="appointment[address]" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row"><label class="col-lg-2 col-form-label">Phone</label>

                                <div class="col-lg-10"><input type="text" placeholder="Phone" name="appointment[phone]" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row"><label class="col-lg-2 col-form-label">Service</label>

                                <div class="col-lg-10"><input type="text" placeholder="Service" name="appointment[service]" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-sm btn-success" type="submit">Submit</button>
                                    <a class="btn btn-sm btn-info" type="button" href="{{ route('appointment') }}">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('pagescript')
    <script>
    $().ready(function() {
        // validate the comment form when it is submitted
        $(".validate").validate();
    });
    </script>
@endsection
