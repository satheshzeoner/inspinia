
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <title>Physio MedCare</title>

    <link rel="shortcut icon" href="{{ url('theme/img/logo.png') }}" type="image/x-icon"/>

    <link href="{{ asset('theme/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <!-- Toastr style -->
    <link href="{{ asset('theme/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">

    <!-- Gritter -->
    <link href="{{ asset('theme/js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet">

    <link href="{{ asset('theme/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/css/style.css') }}" rel="stylesheet">

    <link href="{{ asset('theme/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">

</head>

<body>
<div id="wrapper">

    @include('layouts.leftmenu')

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
{{--                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>--}}
{{--                    <form role="search" class="navbar-form-custom" action="search_results.html">--}}
{{--                        <div class="form-group">--}}
{{--                            <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">--}}
{{--                        </div>--}}
{{--                    </form>--}}
                </div>
                <ul class="nav navbar-top-links navbar-right">

                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i> {{ __('Log out') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </li>
                    <li>
                        <a class="right-sidebar-toggle">
{{--                            <i class="fa fa-tasks"></i>--}}
                        </a>
                    </li>
                </ul>

            </nav>
        </div>

        @yield('content')

        <div class="footer">
            <div class="float-right">
                10GB of <strong>250GB</strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> Example Company &copy; 2014-2018
            </div>
        </div>
    </div>
</div>

<!-- Mainly scripts -->
<script src="{{ asset('theme/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('theme/js/popper.min.js') }}"></script>
<script src="{{ asset('theme/js/bootstrap.js') }}"></script>
<script src="{{ asset('theme/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('theme/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

<!-- Flot -->
<script src="{{ asset('theme/js/plugins/flot/jquery.flot.js') }}"></script>
<script src="{{ asset('theme/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
<script src="{{ asset('theme/js/plugins/flot/jquery.flot.spline.js') }}"></script>
<script src="{{ asset('theme/js/plugins/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('theme/js/plugins/flot/jquery.flot.pie.js') }}"></script>

<!-- Peity -->
<script src="{{ asset('theme/js/plugins/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('theme/js/demo/peity-demo.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ asset('theme/js/inspinia.js') }}"></script>
<script src="{{ asset('theme/js/plugins/pace/pace.min.js') }}"></script>

<!-- jQuery UI -->
<script src="{{ asset('theme/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

<!-- GITTER -->
<script src="{{ asset('theme/js/plugins/gritter/jquery.gritter.min.js') }}"></script>

<!-- Sparkline -->
<script src="{{ asset('theme/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>

<!-- Sparkline demo data  -->
<script src="{{ asset('theme/js/demo/sparkline-demo.js') }}"></script>

<!-- ChartJS-->
<script src="{{ asset('theme/js/plugins/chartJs/Chart.min.js') }}"></script>

<!-- Toastr -->
<script src="{{ asset('theme/js/plugins/toastr/toastr.min.js') }}"></script>

{{-- Datatable --}}
<script src="{{ asset('theme/js/plugins/dataTables/datatables.min.js') }}"></script>
<script src="{{ asset('theme/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>

{{-- Form Validation--}}
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>

<script>
    /* Constant Url Path */
    const base_url = '{{ url('') }}';

    $(document).ready(function () {
        toastr.options.timeOut = 1500; // 1.5s
        @if(Session::has('store'))
        toastr.success('{{Session::get('store')}}');
        @endif

        @if(Session::has('update'))
        toastr.success('{{Session::get('update')}}');
        @endif

        @if(Session::has('destroy'))
        toastr.success('{{Session::get('destroy')}}');
        @endif
    });
</script>



@yield('pagescript')

<script>
    $(document).ready(function() {
        // setTimeout(function() {
        //     toastr.options = {
        //         closeButton: true,
        //         progressBar: true,
        //         showMethod: 'slideDown',
        //         timeOut: 4000
        //     };
        //     toastr.success('Responsive Admin Theme', 'Welcome to INSPINIA');
        //
        // }, 1300);


        var data1 = [
            [0,4],[1,8],[2,5],[3,10],[4,4],[5,16],[6,5],[7,11],[8,6],[9,11],[10,30],[11,10],[12,13],[13,4],[14,3],[15,3],[16,6]
        ];
        var data2 = [
            [0,1],[1,0],[2,2],[3,0],[4,1],[5,3],[6,1],[7,5],[8,2],[9,3],[10,2],[11,1],[12,0],[13,2],[14,8],[15,0],[16,0]
        ];
        $("#flot-dashboard-chart").length && $.plot($("#flot-dashboard-chart"), [
                data1, data2
            ],
            {
                series: {
                    lines: {
                        show: false,
                        fill: true
                    },
                    splines: {
                        show: true,
                        tension: 0.4,
                        lineWidth: 1,
                        fill: 0.4
                    },
                    points: {
                        radius: 0,
                        show: true
                    },
                    shadowSize: 2
                },
                grid: {
                    hoverable: true,
                    clickable: true,
                    tickColor: "#d5d5d5",
                    borderWidth: 1,
                    color: '#d5d5d5'
                },
                colors: ["#1ab394", "#1C84C6"],
                xaxis:{
                },
                yaxis: {
                    ticks: 4
                },
                tooltip: false
            }
        );

        var doughnutData = {
            labels: ["App","Software","Laptop" ],
            datasets: [{
                data: [300,50,100],
                backgroundColor: ["#a3e1d4","#dedede","#9CC3DA"]
            }]
        } ;


        var doughnutOptions = {
            responsive: false,
            legend: {
                display: false
            }
        };


        var ctx4 = document.getElementById("doughnutChart").getContext("2d");
        new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});

        var doughnutData = {
            labels: ["App","Software","Laptop" ],
            datasets: [{
                data: [70,27,85],
                backgroundColor: ["#a3e1d4","#dedede","#9CC3DA"]
            }]
        } ;


        var doughnutOptions = {
            responsive: false,
            legend: {
                display: false
            }
        };


        var ctx4 = document.getElementById("doughnutChart2").getContext("2d");
        new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});

    });
</script>
</body>
</html>
