<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <img alt="image" class="rounded-circle" src="{{ asset('theme/img/profile_small.jpg') }}"/>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="block m-t-xs font-bold">{{ ucfirst(Auth::user()->name) }}</span>
                        <span class="text-muted text-xs block">{{ ucfirst(Auth::user()->email) }} <b class="caret"></b></span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
{{--                        <li><a class="dropdown-item" href="profile.html">Profile</a></li>--}}
{{--                        <li><a class="dropdown-item" href="contacts.html">Contacts</a></li>--}}
{{--                        <li><a class="dropdown-item" href="mailbox.html">Mailbox</a></li>--}}
                        <li class="dropdown-divider"></li>
                        <li>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li class="{{ request()->is('home') ? 'active' : '' }}">
                <a href="{{ route('home') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <li class="{{ request()->is('appointment/*') ? 'active' : '' }}">
                <a href="{{ route('appointment') }}"><i class="fa fa-calendar-o"></i> <span class="nav-label">Appointment</span></a>
            </li>
            <li class="{{ request()->is('services/*') ? 'active' : '' }}">
                <a href="{{ route('services') }}"><i class="fa fa-edit"></i> <span class="nav-label">Service</span></a>
            </li>
            <li class="{{ request()->is('bannerimage/*') ? 'active' : '' }}">
                <a href="{{route('bannerimage')}}"><i class="fa fa-file-image-o"></i> <span class="nav-label">Banner Image</span></a>
            </li>
            <li class="{{ request()->is('user/*') ? 'active' : '' }}">
                <a href="{{ route('user') }}"><i class="fa fa-user"></i> <span class="nav-label">User</span></a>
            </li>
        </ul>

    </div>
</nav>
