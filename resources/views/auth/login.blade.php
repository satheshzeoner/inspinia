<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Physio MedCare</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ url('theme/img/logo.png') }}" type="image/x-icon"/>

    <link href="{{ url('theme/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('theme/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ url('theme/css/animate.css') }}" rel="stylesheet">
    <link href="{{ url('theme/css/style.css') }}" rel="stylesheet">

</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>

            <img src="{{ url('theme/img/logo.png') }}" alt="" width="80%">

        </div>
    {{--        <h3>Welcome to IN+</h3>--}}
    <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
        </p>
        <p>Login in. To see it in action.</p>
        <form class="m-t" role="form" method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group">
                <input type="text" class="form-control @error('phone_number') is-invalid @enderror" placeholder="Phone Number" name="phone_number" required="">

                @error('phone_number')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <input type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" required="">

                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

        </form>
        {{--        <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>--}}
    </div>
</div>

<!-- Mainly scripts -->
<script src="{{ url('theme/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ url('theme/js/popper.min.js') }}"></script>
<script src="{{ url('theme/js/bootstrap.js') }}"></script>

</body>

</html>
